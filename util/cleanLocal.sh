#!/bin/bash

# =====> clear out run data =====>
echo "DEPLOY:: clearing out old run data"
# might want to move this logic to 'cleanLocal.sh' and 'cleanAll.sh'
sudo rm data/*
sudo rm log/*

